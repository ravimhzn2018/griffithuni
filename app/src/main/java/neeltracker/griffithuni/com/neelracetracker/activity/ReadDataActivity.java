package neeltracker.griffithuni.com.neelracetracker.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import neeltracker.griffithuni.com.neelracetracker.R;
import neeltracker.griffithuni.com.neelracetracker.utils.Constants;
import neeltracker.griffithuni.com.neelracetracker.utils.Utility;

public class ReadDataActivity extends AppCompatActivity {

    TextView tvMaximumSpeed, tvMinimumSpeed, tvAverageSpeed, tvTotalDistance,
            tvTotalTime, tvMaximumAlt, tvMinimumAlt, tvGainAlt, tvLossAlt;
    ArrayList<Double> arrLatitude;
    ArrayList<Double> arrLongitude;
    ArrayList<Double> arrAltitude;
    ArrayList<Double> arrSpeed;
    ArrayList<String> arrTime;
    GraphView graphView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_data);
        initVariables();
        initViews();
        if (!checkReadPermissions()) {
            requestPermissions();
        } else {
            readData();
        }
    }

    private void initVariables() {
        arrLatitude = new ArrayList<>();
        arrLongitude = new ArrayList<>();
        arrAltitude = new ArrayList<>();
        arrSpeed = new ArrayList<>();
        arrTime = new ArrayList<>();
    }

    private void initViews() {
        /*
        TextView tvMaximumSpeed, tvMinimumSpeed, tvAverageSpeed, tvTotalDistance,
            tvTotalTime, tvMaximumAlt, tvMinimumAlt, tvGainAlt, tvLossAlt;
         */
        tvMaximumSpeed = (TextView) findViewById(R.id.tvMaximumSpeed);
        tvMinimumSpeed = (TextView) findViewById(R.id.tvMinimumSpeed);
        tvAverageSpeed = (TextView) findViewById(R.id.tvAverageSpeed);
        tvTotalDistance = (TextView) findViewById(R.id.tvTotalDistance);
        tvTotalTime = (TextView) findViewById(R.id.tvTotalTime);
        tvMaximumAlt = (TextView) findViewById(R.id.tvMaximumAlt);
        tvMinimumAlt = (TextView) findViewById(R.id.tvMinimumAlt);
        tvGainAlt = (TextView) findViewById(R.id.tvGainAlt);
        tvLossAlt = (TextView) findViewById(R.id.tvLossAlt);
        graphView = (GraphView) findViewById(R.id.graph);
    }

    private void populateGraph(ArrayList<Double> arrAlt) {
        GraphView graph = (GraphView) findViewById(R.id.graph);

        DataPoint[] dp = new DataPoint[arrAlt.size()];
        for (int i = 0; i < arrAlt.size(); i++) {
            dp[i] = new DataPoint(i, arrAlt.get(i));
        }

//        new DataPoint[]{
//                new DataPoint(0, 1),
//                new DataPoint(1, 5),
//                new DataPoint(2, 3),
//                new DataPoint(3, 2),
//                new DataPoint(4, 6)
//        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(dp);
        graph.addSeries(series);
    }

    private void requestPermissions() {
        Log.i("Location::", "Requesting permission");
        int PERMISSION_READ = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (PERMISSION_READ != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    Constants.PERMISSION_CODE);
        }
    }

    private boolean checkReadPermissions() {
        int PERMISSION_READ_STORAGE = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        return PERMISSION_READ_STORAGE == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    readData();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utility.showShortToast(ReadDataActivity.this, "Read Storage Permission Denied. Please provide permission");
                }
                return;
            }
        }
    }

    private void readData() {// Load XML for parsing.
        try {
            int permissionCheck = ContextCompat.checkSelfPermission(ReadDataActivity.this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "NeelRaceTracker";
                File file = new File(storagePath, MainActivity.xmlFile);
                Log.i("FileTests", "file is '" + file.getAbsolutePath() + "'");
                Log.i("FileTests", "file exists: " + file.exists());
                file.setReadable(true, false);
                Log.i("FileTests", "app can read file: " + file.canRead());
                FileInputStream fis = new FileInputStream(file);

//                InputStream is = getAssets().open(rootPath); //--This line will only work if you save file in internal storage.

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fis);

                Element element = doc.getDocumentElement();
                element.normalize();

                NodeList nList = doc.getElementsByTagName("params");

                for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element2 = (Element) node;
                        arrLatitude.add(Double.parseDouble(getValue("latitude", element2)));
                        arrLongitude.add(Double.parseDouble(getValue("longitude", element2)));
                        arrAltitude.add(Double.parseDouble(getValue("altitude", element2)));
                        arrSpeed.add(Double.parseDouble(getValue("speed", element2)));
                        arrTime.add(getValue("time", element2));
                     }
                }

                calculateAndDisplay();

            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.showShortToast(ReadDataActivity.this, "Failed to read\n" + e.getLocalizedMessage()
                    + "\n" + e.getMessage());
        }
    }

    private void calculateAndDisplay() {
        //Calculating Time
        try {
            tvTotalTime.setText(Utility.totalTime(arrTime.get(0), arrTime.get(arrTime.size() - 1)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Calculating Distance
        tvTotalDistance.setText(Utility.distanceInMeters(arrLatitude.get(0), arrLongitude.get(0),
                arrLatitude.get(arrLatitude.size() - 1), arrLongitude.get(arrLongitude.size() - 1)));
        //Speed
        tvMaximumSpeed.setText(String.valueOf(Utility.maxSpeed(arrSpeed)));
        tvMinimumSpeed.setText(String.valueOf(Utility.minSpeed(arrSpeed)));
        tvAverageSpeed.setText(String.valueOf(Utility.averageSpeed(arrSpeed)));
        //Altitude
        tvMaximumAlt.setText(String.valueOf(Utility.maxAltitude(arrAltitude)));
        tvMinimumAlt.setText(String.valueOf(Utility.minAltitude(arrAltitude)));

        populateGraph(arrAltitude);
    }

    private static String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }
}
