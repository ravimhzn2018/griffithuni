package neeltracker.griffithuni.com.neelracetracker.activity;

import android.Manifest;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import neeltracker.griffithuni.com.neelracetracker.R;
import neeltracker.griffithuni.com.neelracetracker.utils.Constants;
import neeltracker.griffithuni.com.neelracetracker.utils.Utility;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {

    Button buttonStart, buttonStop, buttonNext;
    TextView tvLat, tvLon;
    //Android Location Provider
    LocationManager locationManager;
    private Location location;
    private static final String TAG = "MainActivity";
    boolean gpsStat = false;
    boolean locationGranted = false;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
    private static final long MIN_TIME_BW_UPDATES = 5000; //5 seconds


    private double LATITUDE;
    private double LONGITUDE;
    private double ALTITUDE;
    private double SPEED;

    ArrayList<Double> arrLatitude;
    ArrayList<Double> arrLongitude;
    ArrayList<Double> arrAlttitude;
    ArrayList<Double> arrSpeed;
    ArrayList<String> arrTime;
    public static final String xmlFile = "data.xml";

    @Override
    protected void onStart() {
        super.onStart();
        arrLatitude = new ArrayList<>();
        arrLongitude = new ArrayList<>();
        arrAlttitude = new ArrayList<>();
        arrSpeed = new ArrayList<>();
        arrTime = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

    }

    private void getLocationViaLocationManager() {
        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES,
                    (LocationListener) this);
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (canGetLocation()) {
                if (location != null) {
                    try {
                        LONGITUDE = location.getLongitude();
                        LATITUDE = location.getLatitude();
                        ALTITUDE = location.getAltitude();
                        SPEED = location.getSpeed();
                        tvLat.setText(String.valueOf(LATITUDE));
                        tvLon.setText(String.valueOf(LONGITUDE));

                        //add it into arrayList
                        arrLatitude.add(LATITUDE);
                        arrLongitude.add(LONGITUDE);
                        arrAlttitude.add(ALTITUDE);
                        arrSpeed.add(SPEED);
                        arrTime.add(Utility.getCurrentTime());
                    } catch (NullPointerException n) {
                    }

                }
            } else {
                showGPSDisabledAlertToUser();
            }
        }
    }

    public boolean canGetLocation() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        criteria.setCostAllowed(false);
        String providerName = locationManager.getBestProvider(criteria, true);
        if (providerName != null) {
            return true;
        }
        return false;
    }

    //Requests permission with users
    private void requestPermissions() {
        Log.i("Location::", "Requesting permission");
        int locationPermission_COARSE = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int locationPermission_FINE = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int locationPermission_WRITE = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission_COARSE != PackageManager.PERMISSION_GRANTED
                && locationPermission_FINE != PackageManager.PERMISSION_GRANTED
                && locationPermission_WRITE != PackageManager.PERMISSION_GRANTED
                ) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    Constants.PERMISSION_CODE);
        }
    }

    //Initializing the variables
    private void initViews() {
        tvLat = (TextView) findViewById(R.id.tvLat);
        tvLon = (TextView) findViewById(R.id.tvLon);
        buttonStart = (Button) findViewById(R.id.buttonStart);
        buttonStop = (Button) findViewById(R.id.buttonStop);
        buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonStart.setOnClickListener(this);
        buttonStop.setOnClickListener(this);
        buttonNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonStart:
                getLocationViaLocationManager();
                buttonStart.setText("Tracking...");
                buttonStart.setEnabled(false);
                buttonNext.setEnabled(false);
                //saveToGPX();
                break;
            case R.id.buttonStop:
                stopLoationTracking();
                break;
            case R.id.buttonNext:
                gotoNextActivity();
                break;
        }
    }

    //Goes to Next Activity
    private void gotoNextActivity() {
        startActivity(new Intent(MainActivity.this, ReadDataActivity.class));
    }

    /*
    OnLocationChanged provides location updates whenever LocationListener listens for new coordinates.
     */
    @Override
    public void onLocationChanged(Location location) {
        LONGITUDE = location.getLongitude();
        LATITUDE = location.getLatitude();
        ALTITUDE = location.getAltitude();
        SPEED = location.getSpeed();

        //add it into arrayList
        arrLatitude.add(LATITUDE);
        arrLongitude.add(LONGITUDE);
        arrAlttitude.add(ALTITUDE);
        arrSpeed.add(SPEED);
        arrTime.add(Utility.getCurrentTime());

        tvLat.setText(String.valueOf(LATITUDE));
        tvLon.setText(String.valueOf(LONGITUDE));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("GPS is not Enabled!")
                .setMessage("Do you want to turn on GPS?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    /*
    Will check if the user has already provided permissions. If yes --> Continue otherwise return.
     */
    private boolean checkPermissions() {
        int PERMISSION_LOCATION = ActivityCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        int PERMISSION_WRITE_STORAGE = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (PERMISSION_LOCATION == PackageManager.PERMISSION_GRANTED
                && PERMISSION_WRITE_STORAGE == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    /*
    Once the user has provided permission continue with the process onwards..!!
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    locationGranted = true;
                    getLocationViaLocationManager();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    locationGranted = false;
                    stopLoationTracking();
                }
                return;
            }

        }
    }


    private void stopLoationTracking() {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
            locationManager = null;
            saveToGPX();
            //clear data from arraylist
            arrLatitude.clear();
            arrLongitude.clear();
            arrSpeed.clear();
            arrAlttitude.clear();
            arrTime.clear();
        }
    }

    /*
    We have to call this function inorder to stop location updates if activity has already been closed.
     */
    @Override
    protected void onPause() {
        super.onPause();
        stopLoationTracking();
        Log.i(TAG, "onPause, Location Stopped");
    }

    /*
    Creates and saves to XML/ GPX file.
     */
    public void saveToGPX() {
        try {
            String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            String rootPath = storagePath + "/NeelRaceTracker";
            File root = new File(rootPath);
            if (!root.mkdirs()) {
                Log.i("Test", "This path already exist: " + root.getAbsolutePath());
            }
            File file = new File(rootPath + "/" + xmlFile);
            int permissionCheck = ContextCompat.checkSelfPermission(MainActivity.this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                if (!file.createNewFile()) {
                    Log.i("Test", "This file is already exist: " + file.getAbsolutePath());
                }
            }

            FileOutputStream fileos = new FileOutputStream(file);
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag(null, "gpx");

            for (int i = 0; i < arrLatitude.size(); i++) {
                xmlSerializer.startTag(null, "params");

                xmlSerializer.startTag(null, "latitude");
                xmlSerializer.text(String.valueOf(arrLatitude.get(i)));
                xmlSerializer.endTag(null, "latitude");

                xmlSerializer.startTag(null, "longitude");
                xmlSerializer.text(String.valueOf(arrLongitude.get(i)));
                xmlSerializer.endTag(null, "longitude");

                xmlSerializer.startTag(null, "altitude");
                xmlSerializer.text(String.valueOf(arrAlttitude.get(i)));
                xmlSerializer.endTag(null, "altitude");

                xmlSerializer.startTag(null, "speed");
                xmlSerializer.text(String.valueOf(arrSpeed.get(i)));
                xmlSerializer.endTag(null, "speed");

                xmlSerializer.startTag(null, "time");
                xmlSerializer.text(arrTime.get(i));
                xmlSerializer.endTag(null, "time");

                xmlSerializer.endTag(null, "params");
            }

            xmlSerializer.endTag(null, "gpx");
            xmlSerializer.endDocument();
            xmlSerializer.flush();

            String dataWrite = writer.toString();
            fileos.write(dataWrite.getBytes());
            fileos.close();
            Utility.showShortToast(MainActivity.this, "GPX File Created!!");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            buttonStart.setText("Start Tracking");
            buttonStart.setEnabled(true);
            buttonNext.setEnabled(true);
        }

    }

    public boolean doesFileExist() {
        try {
            int permissionCheck = ContextCompat.checkSelfPermission(MainActivity.this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "NeelRaceTracker";
                File file = new File(storagePath, MainActivity.xmlFile);
                Log.i("FileTests", "file is '" + file.getAbsolutePath() + "'");
                Log.i("FileTests", "file exists: " + file.exists());
                if (file.exists()) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.showShortToast(MainActivity.this, "Failed to read\n" + e.getLocalizedMessage()
                    + "\n" + e.getMessage());
        }
        return false;

    }


}
