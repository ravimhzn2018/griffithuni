package neeltracker.griffithuni.com.neelracetracker.utils;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import neeltracker.griffithuni.com.neelracetracker.R;

public class Utility {

    public static void showLongToast(Context context, String message) {
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.from(context).inflate(R.layout.custom_toast, null);
            TextView text = (TextView) layout.findViewById(R.id.tvToastMsg);
            text.setText(message);
            Toast toast = new Toast(context);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        } catch (NullPointerException n) {

        }
    }

    /*
    Custom Short 2 sec. Toast
     */
    public static void showShortToast(Context context, String message) {
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.from(context).inflate(R.layout.custom_toast, null);
            TextView text = (TextView) layout.findViewById(R.id.tvToastMsg);
            text.setText(message);
            Toast toast = new Toast(context);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } catch (NullPointerException n) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    public static String getCurrentTime() {
        return new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    /*
    Calculation of total time of travel
     */
    public static String totalTime(String startDate, String endDate) throws ParseException {
        Date date1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(startDate);
        Date date2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(endDate);
        //long differenceInSeconds = TimeUnit.MILLISECONDS.toSeconds(date2.getTime() - date1.getTime());
        long milliseconds = date2.getTime() - date1.getTime();


        int seconds = (int) (milliseconds / 1000) % 60;
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);


        return hours + ":" + minutes + ":" + seconds;
    }

    public static String distanceInMeters(double lat1, double lon1, double lat2, double lon2) {
        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lon1);

        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lon2);


        return String.valueOf(loc1.distanceTo(loc2));


    }

    public static double maxSpeed(ArrayList<Double> arrMaxSpeed) {
        double max = 0;
        for (double i : arrMaxSpeed) {
            if (max < i) {
                max = i;
            }
        }
        return max;
    }

    public static double minSpeed(ArrayList<Double> arrMinSpeed) {
        double min = arrMinSpeed.get(0);
        for (double i : arrMinSpeed) {
            if (min > i) {
                min = i;
            }
        }
        return min;
    }

    public static double averageSpeed(ArrayList<Double> arrSpeed) {
        return (maxSpeed(arrSpeed) + minSpeed(arrSpeed)) / 2;
    }


    public static double maxAltitude(ArrayList<Double> arrAlt) {
        double max = 0;
        for (double i : arrAlt) {
            if (max < i) {
                max = i;
            }
        }
        return max;
    }

    public static double minAltitude(ArrayList<Double> arrAlt) {
        double min = arrAlt.get(0);
        for (double i : arrAlt) {
            if (min > i) {
                min = i;
            }
        }
        return min;
    }
}
